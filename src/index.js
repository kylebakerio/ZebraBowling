const express     = require('express');
const app         = express();
const bodyParser  = require('body-parser');
const morgan      = require('morgan');
const GameModel   = require('./model/gameModel.js');
const path        = require('path');
const cors        = require('cors');
const PORT        = process.env.PORT || 3018;

app.use(bodyParser.json());
// app.use(cors());
app.use(express.static(__dirname + '/client'));
app.use('/node_modules', express.static(path.join(__dirname, '../node_modules')));
app.use(morgan('dev'));

const server = app.listen(PORT, () => console.log('Zebra Bowling Service Online @ localhost:'+server.address().port));

// for heroku warm-up
app.get('/warmup', cors(), (req, res) => {
  res.send("warming up");
});

// Takes an array of names, creates a locally stored game object, and returns 
// user-friendly form of that object which contains the ID needed to access it.
//
app.post('/games', (req, res) => {
  // example expected input: req.body.names === ["John", "Sarah"]
  if ( !Array.isArray(req.body.names) || req.body.names.length < 1 ) {
    console.error("req.body.names is empty or not an array, 400");
    res.status(400).send({message: "Invalid names list. Must send key 'names' with a value of an array of strings"});
  }
  else {
    var gameID = GameModel.new(req.body.names, req.ip);
    res.status(201).send({game: GameModel.plainScores(gameID)});
    // If >5k games, set timer to delete after 6 hours to prevent server death.
    if (GameModel.gameCount > 5000) {
      setTimeout(() => GameModel.games[gameID] = undefined, 21600000);
    }
  }
});

// Returns all game ID's.
//
app.get('/serverCheck', (req, res) => {
  res
  .status(200)
  .send(`<pre>${
    JSON.stringify(
      Object.values(GameModel.games).map(
        game => ({
          ip: game.ip,
          date: game.date,
          players: game.players.map(playerObj => playerObj.name),
        })
      ),
      null,
      2
    )
    }</pre>`
  );
});

// Returns all game ID's.
//
app.get('/games', (req, res) => {
  res.status(200).send(JSON.stringify(GameModel.games, null, 2));
});

// Takes in an ID and returns the game state object, with integers instead
// of functions
app.get('/games/:id', (req,res) => {
  var gameID = Number(req.params.id);
  
  if (typeof GameModel.games[gameID] === "undefined") 
    res.status(404).send({message: "game does not exist"});
  else res.send(GameModel.plainScores(gameID));
});

// RESTful and idempotent; takes in data on individual rolls that are contained in 
// request body (or constructed by client), returns the round that has been updated. 
// Can create or update, so can be used for 'undo' and 'edit' functions by clients.
// HATEOAS / Richardson Maturity Level 3
//

app.put('/games/:id/players/:player/rounds/:round/rolls/:roll', (req, res) => {
  // Example: req.body === {pins: 12}
  var gameID = Number(req.params.id);
  var player = Number(req.params.player);
  var round  = Number(req.params.round);
  var roll   = Number(req.params.roll);
  var pins   = Number(req.body.pins);

  console.log("Updating, via RESTful endpoint, game:", gameID, player, round, roll, pins);

  if (typeof GameModel.games[gameID] === "undefined") {
    res.status(404).send({message: "Game doesn't exist."});
    console.log("Game doesn't exist -- did the server restart, or was client left open too long? Games are temporary.");
  }
  else {
    console.log("before", {round: GameModel.games[gameID].players[player].rounds[round], nextRoll: GameModel.games[gameID].nextRoll})

    GameModel.updateScore(gameID, player, round, roll, pins);
    // More error handling could go here.
    console.log("after", {round: GameModel.games[gameID].players[player].rounds[round], nextRoll: GameModel.games[gameID].nextRoll})
    res.send({round: GameModel.games[gameID].players[player].rounds[round], nextRoll: GameModel.games[gameID].nextRoll});
  }
});

app.delete('/games/:id', (req,res) => {
  console.log('trying to delete this game:', GameModel.games[gameID] );
  var gameID = Number(req.params.id);

  if (isNaN(gameID)) res.status(400).send({message: "Not a gameID, please send a valid (numerical) gameID."});
  // else if (typeof GameModel.games[gameID] === "undefined") 
  //   res.status(404).send({
  //     message: "Cannot be deleted, because it does not exist. Perhaps you already deleted it? Note: Games are deleted after 6 hours. "
  //   });
  else {
  // GameModel.games[gameID] = undefined;
  // console.log("Deleted game at index: ", gameID);
    res.status(200).send({
      message: "Thanks for playing!",
      comment:"Note: deleting games is currently disabled. Games are only deleted on server reset.",
    }); 
  }
});
