GameModel = {};
GameModel.games = {};
GameModel.gameCount = 0;

// Creates a new game object, which contains a 'players' object property,
// which contains a 'rounds' array property, which contains subarrays 
// for every around, which by default contain functions for every roll
// that return "-". See bottom of this file for an example.

GameModel.new = function (names, ip) {
  var games = this.games;

  var gameID = ++this.gameCount;
  var game = {
    ip,
    date: Date(),
    players: [],
    nextRoll: {
      gameID,
      player: 0,
      round: 0,
      roll: 0,
      maxPinsNextRoll: 10,
    },
  };

  names.forEach(name => { 
    var emptyRounds = [];
    for (var i = 0; i < 10; i++) {
      emptyRounds.push([() => "-",() => "-"]);
    }
    game["players"].push( {name, "rounds": emptyRounds} );
  });

  games[gameID]= game;
  console.log(`generated new game ${game.nextRoll.gameID}: ${JSON.stringify(game)}`);

  return game.nextRoll.gameID;
};

var throttleImpossibleScores = function (game, player, round, roll, pins) {
  // After having implemented nextRoll.maxPinsNextRoll, this isn't possible unless 'cheating' is attempted.
  // Still, prevents buggy potential output if something goes wrong or if API is abused.
  // Forbids higher total pin counts than 10 between two rolls in a round, unless final round

  var secondRoll = roll  === 1;
  var thirdRoll  = roll  === 2; // strike or spare in last round
  var lastRound  = round === 9;
  var combinedRollsOverTen = game.players[player].rounds[round][0]() + pins > 10;
  var firstRollOfLastRoundIsNotTen = round === 9 && game.players[player].rounds[9][0]() !== 10;
  
  if (combinedRollsOverTen && secondRoll && (!lastRound || firstRollOfLastRoundIsNotTen)) {
    // If this isn't first bonus roll of a strike in round 9
    // Reduce to max legal playable for this roll
      pins = 10 - game.players[player].rounds[round][0]();
      console.error(`HAD AN IMPOSSIBLE SCORE: 1 | args: ${JSON.stringify(arguments)}`);
  }

  // Edge case:
  // Forbids higher total pin counts than 10 between two bonus rolls in final round, 
  // unless the first bonus roll was also a strike, or third is bonus from spare.
  //
  else if ( lastRound && thirdRoll && 
    (game.players[player].rounds[round][1]() < 10) &&
    (game.players[player].rounds[round][1]() + pins > 10) &&
    (game.players[player].rounds[round][0]() + game.players[player].rounds[round][1]() !== 10)) {
      pins = 10 - game.players[player].rounds[round][1]();
      console.error(`HAD AN IMPOSSIBLE SCORE: 2 | args: ${JSON.stringify(arguments)}`);
  }

  return pins;
};

GameModel.updateScore = function (gameID, player, round, roll, pins) {
  var games = this.games;

  pins = throttleImpossibleScores(games[gameID], player, round, roll, pins);

  // Within the array for this round, assign the index that corresponds to this 
  // 'roll' to a function that returns the face value when called
  games[gameID].players[player].rounds[round][roll] = () => pins;

  var isStrike = roll === 0 && pins === 10;
  var lastRound = round === 9;
  var isSpare = roll === 1 && games[gameID].players[player].rounds[round][0]() + games[gameID].players[player].rounds[round][1]() === 10;
  if ((isStrike || isSpare) && lastRound) {
      // Add third function into this round that returns bonus ball  OR 
      // "?" if not yet played
      //
      games[gameID].players[player].rounds[round][2] = () => "-";
  }
  else if (isStrike && !lastRound) {
      // Redefine second roll in this round as function that returns 
      // bonus ball 1 OR "?" if not yet played
      games[gameID].players[player].rounds[round][1] = 
        () => {
          return typeof games[gameID].players[player].rounds[round+1][0]() === "number" ?
            games[gameID].players[player].rounds[round+1][0]() : "?";
        }

      // Add third function into this round that returns bonus ball 2 OR 
      // "?" if not yet played
      //
      games[gameID].players[player].rounds[round][2] = 
        () => {
          return typeof games[gameID].players[player].rounds[round+1][1]() === "number" ?
            games[gameID].players[player].rounds[round+1][1]() : "?";
        }
  }
  else if (isSpare && !lastRound) {
      // push function into this round that returns bonus ball 1 OR "?" if not yet played
      games[gameID].players[player].rounds[round][2] =
        () => {
          return typeof games[gameID].players[player].rounds[round+1][0]() === "number" ?
            games[gameID].players[player].rounds[round+1][0]() : "?";
        }
      // else if last round nothing, will be handled as by findNextRoll.
  }

  return GameModel.findNextRoll(gameID, player, round, roll, pins);
};

// Returns the almost same game object, with the subarrays containing rolls 
// populated with corresponding integers instead of functions.
//
GameModel.plainScores = function (gameID) {
  var games = this.games;

  var game = { nextRoll: games[gameID].nextRoll, players: [] };

  // For each player
  games[gameID].players.forEach(function(player, j){              
    game.players[j] = { name: player.name, total: 0, rounds: [] };

    // For each round
    for (var i = 0; i < 10; i++) {  

      // Replace the function with either its value, or 0 if not a number.
      game.players[j].rounds[i] = games[gameID].players[j].rounds[i].map((intFunc) => { 
        game.players[j].total += typeof intFunc() === "number" ? intFunc() : 0;
        return intFunc();
      });

    }

  })

  return game;
};

GameModel.findNextRoll = function (gameID, player, round, roll, pins) {
  var games = this.games;

  var nextRoll = {gameID: gameID, gameOver: false, maxPinsNextRoll: 10};

  var isLastRoundSpare = (games[gameID].players[player].rounds[9][0]() + games[gameID].players[player].rounds[9][1]() === 10);
  var isLastRoundStrike = games[gameID].players[player].rounds[9][0]() === 10;
  var isLastRoundSecondStrike = games[gameID].players[player].rounds[9][1]() === 10;

  // If invalid input (expand to be as robust as desired)
  if (gameID > games.length-1 || player > games[gameID].players.length-1 ||
    round > 9 || roll > 2 || pins > 10 || player < 0 || round < 0 || pins < 0 || roll < 0) {
    return "invalid entry";
  }

  // If special case of final round strike or spare, next roll is a bonus second or third roll.
  else if ( round > 8 && (isLastRoundStrike || isLastRoundSpare) && roll < 2 ) {
    nextRoll.roll   = roll + 1;
    nextRoll.round  = round;
    nextRoll.player = player;
    if (isLastRoundStrike && roll === 0) {
      nextRoll.maxPinsNextRoll = 10;
    }
    else if (isLastRoundSecondStrike && roll === 1) {
      nextRoll.maxPinsNextRoll = 10;
    }
    else if (isLastRoundSpare) {
      nextRoll.maxPinsNextRoll = 10;
    }
    else if (nextRoll.roll === 0) {
      nextRoll.maxPinsNextRoll = 10;
    }
    else if (nextRoll.roll === 1) {
      nextRoll.maxPinsNextRoll = 10 - pins;
    }
    else {
      console.error(`there shouldn't be another condition... | arguments: ${JSON.stringify(arguments)}`);
    }
  }

  // If not the above and second or third roll of final round of final player, gameover.
  else if (player >= games[gameID].players.length-1 && round > 8 && roll > 0) {
    nextRoll.player = 0;
    nextRoll.round  = 0;
    nextRoll.roll   = 0;
    nextRoll.gameOver = true;
    nextRoll.maxPinsNextRoll = 0;
  }

  // If end of player's turn (via 2 rolls, finished spare, or first roll strike)
  else if (roll === 1 || roll === 2 || (roll === 0 && pins === 10)) {
                        // (or only player)
    var onLastPlayer = games[gameID].players.length === 1 || player === games[gameID].players.length - 1;
    
    // If currently on last player, next is first player's turn; otherwise, next player
    nextRoll.player = onLastPlayer ? 0 : player+1;

    // If currently on last player, starting new round; otherwise, same round
    nextRoll.round = onLastPlayer ? round+1 : round;

    // In all of these cases, next roll is roll 0
    nextRoll.roll = 0;
    nextRoll.maxPinsNextRoll = 10;
  }

  // If first roll of round, and not a strike
  else if (roll === 0) {
    nextRoll.player = player;
    nextRoll.round  = round;
    nextRoll.roll   = 1;
    nextRoll.maxPinsNextRoll = 10 - pins;
  }
  
  games[gameID].nextRoll = nextRoll;
  return nextRoll;
};

module.exports = GameModel;

//
// Sample game data structure created:
// (note that it comes prefilled with functions for at least two 
// rolls per ten rounds per player that return "-", until overwritten)
//

/*
game[0] === { 
  timeoutID: 1234567890,
  nextRoll: {gameID: 1, player: 1, round: 2, roll: 0},
  players: [
    {   // a player
      name: "John",
      rounds: [
        [    // round 1
          () => {return 9}, // an individual roll
          () => {return 1},   // another individual roll (and so on)
          () => {return games[0 <--this game ][0 <--this player ].rounds[1 <--the next round ][0 <--the first roll of that round] (unless undefined, in which case return 0)},
        ],
        [    // round 2
          () => {return 10},
          () => {return games[0].players[0].rounds[2][0] (unless undefined, in which case return 0)},
          () => {return games[0].players[0].rounds[2][1] (unless undefined, in which case return 0)}
        ],
        (...more rounds here)
      ]
    },
    {  // another player
      name: "Sally",
      rounds: [
        [
          () => {return 9}, 
          () => {return 0}   
        ],
        [
          () => {return 1},
          () => {return 9},
          () => {return games[0].players[1].rounds[2][0] (unless undefined, in which case return 0)}
        ],
        (...more rounds here)
      ]
    },
    (...more players here)
  ]
}
*/

