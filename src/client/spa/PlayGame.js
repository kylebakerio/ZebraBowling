(function(){
  window.Zebra.Component.PlayGame = {};

  Zebra.Component.PlayGame.view = function(ctrl){  
    return m("",[
      m(".container", [
        m(".row", 
          [
            m("h2.col-xs-12.col-sm-3", {style: "margin-bottom:0px"}, "Next Roll: "),
            m("h2.player-name.col-xs-12.col-sm-9", {style: "margin-bottom:0px; text-overflow: ellipsis;"}, getNextPlayer())
          ]
        ),
        m(".row", [
          m("p.col-xs-4.col-md-2", "Pins Knocked Down: "),
          m(".col-xs-1", generateDropdown()),
        ]),
        m(".clearfix"),
        m(".row", 
          m("button.col-xs-6.col-sm-2.drop-button.btn.btn-lg.btn-success.score-button.submit", 
            {onclick: ctrl.score, type: 'button', "aria-hidden":"true"}, 
            [m("span.glyphicon.glyphicon-ok",{"aria-hidden":"true"}), " SCORE!"]
          )
        ),
        m(".row.space"), 
        m( "", generatePlayerRows(Zebra.Model.Game()) ),
        m(".row",
          [
            m("button.col-xs-6.col-sm-3.drop-button.btn.btn-lg.btn-warning.score-button", 
              {onclick: ctrl.deleteGame, type: 'button', "aria-hidden":"true"}, 
              [m("span.glyphicon.glyphicon-remove",{"aria-hidden":"true"}), " New Game"]
            ),
            generateRandomizerToggleButton(ctrl)
          ]
        ),
      ]),
    ])
  };

  Zebra.Component.PlayGame.controller = function(){
    var ctrl  = this;
    ctrl.game = Zebra.Model.Game;

    ctrl.score = function() {
      var update = ctrl.game().nextRoll;
      var elem = document.getElementById("pins_dropdown");
      update.pins = Number(elem.options[elem.selectedIndex].value); 
      Zebra.Model.scoreREST(update);
    };

    ctrl.randomize = function(){
      Zebra.Model.fullAutoRando(true);
      ctrl.score();
    };

    ctrl.deleteGame = function(){
      if (confirm("Are you sure you want to end this game?")) {
        Zebra.Model.deleteGame();
      }
    }
  };

  function getNextPlayer(ctrl) {
    return Zebra.Model.Game().nextRoll.gameOver ? "Game Over" : Zebra.Model.Game().players[Zebra.Model.Game().nextRoll.player].name;
  }

  function generateRandomizerToggleButton(ctrl) {
    if (!Zebra.Model.fullAutoRando()) {
      return m(
        "button.col-xs-6.col-sm-3.drop-button.btn.btn-lg.btn-danger.score-button", 
        {onclick: ctrl.randomize, type: 'button', "aria-hidden":"true"}, 
        m("span.glyphicon.glyphicon-random.unclicked",{"aria-hidden":"true"}),
      );
    }
    else return m("button.col-xs-6.col-sm-3.drop-button.btn.btn-lg.btn-info.score-button",
      {onclick: () => Zebra.Model.fullAutoRando(false), type: 'button', "aria-hidden":"true"}, 
      m("span.glyphicon.glyphicon-random.clicked",{"aria-hidden":"true"})
    );
  };

  function generateDropdown(nextRoll=Zebra.Model.Game().nextRoll) {
    var maxRoll = nextRoll.maxPinsNextRoll;
    var options = [];

    for (var i = 0; i <= maxRoll; i++) {
      options.push(m(`option[value=${i}]`, i));
    }

    return m("select.dropdown[id='pins_dropdown'][name='pins']", options);
  }

  function generatePlayerRows(game=Zebra.Model.Game()){
    var playerRows = [];

    for (var player = 0; player < game.players.length; player++) {
      playerRow = [];
      playerRow.push(
        [
          m("h4.player-name", game.players[player].name),
          m(".col-xs-12.col-sm-1",
            m("p", "Total: " + game.players[player].total)
          )
        ]
      )
      
      for (var round = 0; round < game.players[player].rounds.length; round++) {
        var scoreBox = [];
        var subtotal = 0;

        for (var roll = 0; roll < game.players[player].rounds[round].length; roll++) {
          if (!game.nextRoll.gameOver && round === game.nextRoll.round && player === game.nextRoll.player && roll === game.nextRoll.roll) {
            var rollClass = "b.points.current-roll.col-xs-3";
          }
          else var rollClass = "p.points.col-xs-3";

          if (roll === 0) rollClass += ".col-xs-offset-1"

          scoreBox.push(
            m(rollClass, " " + game.players[player].rounds[round][roll])
          );

          subtotal += typeof game.players[player].rounds[round][roll] === "number" ? 
            game.players[player].rounds[round][roll] : 0;
        }

        scoreBox.push(
          m("b.col-xs-10.col-xs-offset-1.points", " Total: " + subtotal)
        );

        var scoreBoxClass = ".col-xs-4.col-sm-1.scorebox"
        if (!game.nextRoll.gameOver && round === game.nextRoll.round && player === game.nextRoll.player) 
          scoreBoxClass += ".current-round"; 
        else if (game.players[player].rounds[round][0] === 10) 
          scoreBoxClass += ".strike"; 
        else if (game.players[player].rounds[round][0] + game.players[player].rounds[round][1] === 10) 
          scoreBoxClass += ".spare";

        playerRow.push( m(scoreBoxClass, scoreBox) )
      }

      playerRows.push(
        m(".row.player", playerRow)
      )
    }

    return playerRows;
  }

})()
