Self:
-to deploy, just push to heroku remote. Everything is automated. Log in to heroku for more info.
-to run locally, just `yarn install` and `nodemon src/index.js`

Everyone else: 
check out [my blog post about this project](http://code.blog.kylebaker.io/2018/03/18/zebra-bowling/) to see more.

Some little features added since original deploy around when I wrote that blog post, for fun:
-hitting 'enter' key when adding players adds a player. that's nice.
-randomizer uses more elegant code and structure to function.
-game end is indicated and handled more nicely--says 'game over' instead of first player is next at game end, and first roll box doesn't highlight as if it is next roll now.
-client handles a reset server gracefully.
-server logs ip addresses of users.
-server stores creation timestamp of game.
-server preserves games now, instead of deleting after 6 hours--unless it hits over 5k games. then it will go back to giving them a 6 hour lifespan.
-server api expanded to inform the client of the highest possible next roll.
-client uses that info to dynamically populate the scoring dropdown appropriately.
-randomize button now toggles into a pause button, instead of having pause be a separate button.
-handling for long names on all screen sizes, everywhere names are displayed
-medium screens no longer get uneven height for frame boxes based on contents.
-/serverCheck endpoint

Things the original had:
-virtually identical visual interface
-virtually identical api
-equally bug-free scoring engine
